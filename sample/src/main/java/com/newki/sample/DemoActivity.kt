package com.newki.sample

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.newki.circle_progress.MyCircleProgressView

class DemoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_demo)

        //设置进度
        findViewById<Button>(R.id.set_progress).setOnClickListener {

            findViewById<MyCircleProgressView>(R.id.progress_view).setValue("60", 100f)

        }

    }

}